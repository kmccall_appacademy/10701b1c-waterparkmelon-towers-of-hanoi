class TowersOfHanoi
  attr_accessor :towers

  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    if @towers[to_tower].empty? || @towers[from_tower].last < @towers[to_tower].last
      return true if from_tower.between?(0,2) && to_tower.between?(0,2)
    elsif @towers[from_tower].empty? || @towers[from_tower].last > @towers[to_tower].last
      return false
    end
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end

  def play
    display

    until won?
      puts "From what tower?"
      from_tower = gets.chomp.to_i

      puts "To what tower?"
      to_tower = gets.chomp.to_i

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
      else
        puts "Invalid move. Try again."
      end

      display
    end

    puts "You have won!"
  end

  def display
    puts @towers.to_s
  end
end

if $PROGRAM_NAME == __FILE__
  TowersOfHanoi.new.play
end
